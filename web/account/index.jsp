<%@ page import="domain.Product" %>
<%@ page import="servlet.AccountServlet" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="domain.User" %><%-- Created by IntelliJ IDEA.
  User: haykazp
  Date: 13/01/2022
  Time: 07:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/layouts/header.jsp" %>
<% User user = (User) request.getAttribute("user"); %>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="/">
                Project
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->


                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                         <%= user.getFirst_name() %> <%= user.getLast_name() %>  <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="">Transactions</a>

                            <a class="dropdown-item" href="logout"
                               onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                Logout
                            </a>
                            <form id="logout-form" action="/logout" method="POST" style="display: none;">

                            </form>
                        </div>
                    </li>

                </ul>
            </div>
        </div>
    </nav>

    <main class="py-4">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Product List</div>

                        <div class="card-body">
                            <div class="row">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Id</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Price</th>
                                        <th scope="col">Count</th>
                                        <th scope="col">Code</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <% ArrayList<Product> products = (ArrayList<Product>) request.getAttribute("products"); %>
                                    <% for (Product product : products) { %>
                                    <tr>
                                        <%="<th scope=\"row\">" + product.getId() +"</th>"%>
                                        <%="<th>" + product.getName() +"</th>"%>
                                        <%="<th>" + product.getPrice() +"</th>"%>
                                        <%="<th>" + product.getCount() +"</th>"%>
                                        <%="<th>" + product.getCode() +"</th>"%>
                                        <%="<th><a href=\"#\" class=\"badge badge-warning\">Update</a><a href=\"\" class=\"badge badge-danger\">Delete</a></th>"%>
                                    </tr>
                                    <%}%>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>

<%@ include file="/layouts/footer.jsp" %>