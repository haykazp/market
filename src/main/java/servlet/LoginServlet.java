package servlet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {


        HttpSession session = request.getSession();
        String active = (String)session.getAttribute("active");

        if (active != null){
             response.sendRedirect("http://localhost:8080/account");
        }else  if (active == null){

            RequestDispatcher requestDispatcher = request.getRequestDispatcher("login/index.jsp");
            try {
                requestDispatcher.forward(request,response);
            } catch (ServletException e) {
                e.printStackTrace();
            }
        }



    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response)  {


    }
}
