package servlet;

import domain.Product;
import domain.User;
import service.impl.ProductDaoImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class AccountServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {


        HttpSession session = request.getSession();
        User user = (User)session.getAttribute("user");
        String active = (String)session.getAttribute("active");

        ProductDaoImpl productDao = new ProductDaoImpl();
        ArrayList<Product> products = productDao.getAll();

        request.setAttribute("products", products);

        request.setAttribute("user",user);

        if (active != null){

            try {
                request.getRequestDispatcher("account/index.jsp").forward(request,response);
            } catch (ServletException e) {
                e.printStackTrace();
            }

        }

        response.sendRedirect("http://localhost:8080/login");


    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response)  {


    }
}
