package servlet;

import domain.User;
import dto.UserDto;
import service.impl.UserDaoImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

public class RegisterServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        HttpSession session = request.getSession();
        String active = (String)session.getAttribute("active");

        if (active != null){
            response.sendRedirect("http://localhost:8080/account");
        }else if (active == null){
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("register/index.jsp");
            try {
                requestDispatcher.forward(request,response);
            } catch (ServletException e) {
                e.printStackTrace();
            }
        }

    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response)  {

        UserDto userDto = new UserDto();
        userDto.setFirst_name(request.getParameter("first_name"));
        userDto.setLast_name(request.getParameter("last_name"));
        userDto.setEmail(request.getParameter("email"));
        userDto.setPassword(request.getParameter("password"));

        UserDaoImpl userDao = new UserDaoImpl();

        User user = this.toEntity(userDto);

        if (userDao.create(user)){

          HttpSession session = request.getSession();
          session.setAttribute("user",user);
          session.setAttribute("active","ok");

            try {
                response.sendRedirect("http://localhost:8080/account");
            } catch (IOException e) {
                e.printStackTrace();
            }

        }


    }



    private User toEntity(UserDto userDto){

        User user = new User();
        user.setFirst_name(userDto.getFirst_name());
        user.setLast_name(userDto.getLast_name());
        user.setEmail(userDto.getEmail());
        user.setPassword(userDto.getPassword());

        return user;
    }
}
