package servlet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ProductServlet extends HttpServlet {

    protected  void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("product.jsp");
        try {
            requestDispatcher.forward(request,response);
        } catch (ServletException e) {
            e.printStackTrace();
        }
    }

    protected  void doPost(HttpServletRequest requset, HttpServletResponse response) {

    }

    protected  void doPut(HttpServletRequest requset, HttpServletResponse response){

    }

    protected  void doDelete(HttpServletRequest requset, HttpServletResponse response){

    }
}
