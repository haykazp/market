package servlet;

import domain.Product;
import domain.User;
import service.impl.ProductDaoImpl;
import service.impl.UserDaoImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class AdminServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        UserDaoImpl userDao = new UserDaoImpl();
        ArrayList<User> users =  userDao.getAll();

        ProductDaoImpl productDao = new ProductDaoImpl();
        ArrayList<Product> products = productDao.getAll();

        request.setAttribute("users",users);
        request.setAttribute("products",products);


        RequestDispatcher requestDispatcher = request.getRequestDispatcher("admin/index.jsp");
        try {
            requestDispatcher.forward(request,response);
        } catch (ServletException e) {
            e.printStackTrace();
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)  {


    }
}
