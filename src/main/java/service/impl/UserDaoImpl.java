package service.impl;

import connection.Connector;
import domain.User;
import dto.UserDto;
import service.dao.UserDao;

import java.sql.*;
import java.util.ArrayList;

public class UserDaoImpl implements UserDao {

    public User getById(long id) {

        final String sql = "SELECT * FROM users WHERE id=?";
        User user = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {

            connection = Connector.conn();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setLong(1,id);
            resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                user = new User();
                user.setId(resultSet.getInt("id"));
                user.setFirst_name(resultSet.getString("first_name"));
                user.setLast_name(resultSet.getString("last_name"));
                user.setEmail(resultSet.getString("email"));
                user.setPassword(resultSet.getString("password"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return user;
    }

    public ArrayList<User> getAll() {

        final String sql = "SELECT * FROM users";
        User user = null;
        ArrayList<User> users = new ArrayList<User>();
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {

            connection = Connector.conn();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);

            while (resultSet.next()){
                user = new User();
                user.setId(resultSet.getInt("id"));
                user.setFirst_name(resultSet.getString("first_name"));
                user.setLast_name(resultSet.getString("last_name"));
                user.setEmail(resultSet.getString("email"));
                user.setPassword(resultSet.getString("password"));

                users.add(user);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return users;

    }

    public boolean create(User user) {

        boolean isOk = false;
        int rs = 0;
        final String sql = "INSERT INTO users (first_name,last_name,email,password) VALUES (?,?,?,?)";
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {

            connection = Connector.conn();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,user.getFirst_name());
            preparedStatement.setString(2,user.getLast_name());
            preparedStatement.setString(3,user.getEmail());
            preparedStatement.setString(4,user.getPassword());

            rs = preparedStatement.executeUpdate();

            if (rs == 1)
            {
                isOk = true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return isOk;
    }

    public boolean update(User user, long id) {

        boolean isOk = false;
        int rs =0;
        final String sql = "UPDATE users SET first_name=?,last_name=?,email=?,password=? WHERE id=?";
        Connection connection = null;
        PreparedStatement preparedStatement = null;


        try {

            connection = Connector.conn();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,user.getFirst_name());
            preparedStatement.setString(2,user.getLast_name());
            preparedStatement.setString(3,user.getEmail());
            preparedStatement.setString(4,user.getPassword());
            preparedStatement.setLong(5,id);

            rs = preparedStatement.executeUpdate();

            if (rs == 1)
            {
                isOk = true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return isOk;
    }

    public boolean remove(long id) {

        boolean isOk = false;
        int rs = 0;
        final String sql = "DELETE FROM  users  WHERE id=?";
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {

            connection = Connector.conn();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setLong(1,id);

            rs = preparedStatement.executeUpdate();

            if (rs == 1){
                isOk = true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


        return isOk;
    }
}
