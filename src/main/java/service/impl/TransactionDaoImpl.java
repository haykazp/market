package service.impl;

import connection.Connector;
import domain.Product;
import domain.Transaction;
import domain.User;
import service.dao.TransactionDao;

import java.sql.*;
import java.util.ArrayList;

public class TransactionDaoImpl implements TransactionDao {


    public Transaction getById(long id) {

        final String sql = "SELECT * FROM transactions WHERE id=?";
        User user = null;
        Product product = null;
        Transaction transaction = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {

            connection = Connector.conn();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setLong(1,id);
            resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
               user = new User();
               user.setId(resultSet.getLong("user_id"));

               product = new Product();
               product.setId(resultSet.getLong("product_id"));

               transaction = new Transaction();
               transaction.setId(resultSet.getLong("id"));
               transaction.setUser(user);
               transaction.setProduct(product);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return transaction;
    }

    public ArrayList<Transaction> getAll() {

        final String sql = "SELECT * FROM transactions";
        User user = null;
        Product product = null;
        Transaction transaction = null;
        ArrayList<Transaction> transactions = new ArrayList<Transaction>();
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {

            connection = Connector.conn();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);

            while (resultSet.next()){
                user = new User();
                user.setId(resultSet.getLong("user_id"));

                product = new Product();
                product.setId(resultSet.getLong("product_id"));

                transaction = new Transaction();
                transaction.setId(resultSet.getLong("id"));
                transaction.setUser(user);
                transaction.setProduct(product);

                transactions.add(transaction);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return transactions;
    }

    public boolean create(User user, Product product) {

        boolean isOk = false;
        int rs = 0;
        final String sql = "INSERT INTO transactions (user_id, product_id) VALUES (?,?)";
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {

            connection = Connector.conn();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setLong(1,user.getId());
            preparedStatement.setLong(2,product.getId());

            rs = preparedStatement.executeUpdate();

            if (rs == 1)
            {
                isOk = true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return isOk;
    }

    public boolean update(User user, Product product, long id) {

        boolean isOk = false;
        int rs =0;
        final String sql = "UPDATE transactions SET user_id=?,product_id=? WHERE id=?";
        Connection connection = null;
        PreparedStatement preparedStatement = null;


        try {

            connection = Connector.conn();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setLong(1,user.getId());
            preparedStatement.setLong(2,product.getId());
            preparedStatement.setLong(3,id);


            rs = preparedStatement.executeUpdate();

            if (rs == 1)
            {
                isOk = true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return isOk;
    }

    public boolean delete(long id) {

        boolean isOk = false;
        int rs = 0;
        final String sql = "DELETE FROM  transactions  WHERE id=?";
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {

            connection = Connector.conn();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setLong(1,id);

            rs = preparedStatement.executeUpdate();

            if (rs == 1){
                isOk = true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


        return isOk;
    }
}
