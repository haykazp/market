package service.dao;

import domain.Product;

import java.util.ArrayList;

public interface ProductDao {

  Product getById(long id);
  ArrayList<Product> getAll();
  boolean create(Product product);
  boolean update(Product product,long id);
  boolean remove(long id);

}
