package service.dao;

import domain.User;
import dto.UserDto;

import java.util.ArrayList;

public interface UserDao {

    User getById(long id);
    ArrayList<User> getAll();
    boolean create(User user);
    boolean update(User user,long id);
    boolean remove(long id);
}
